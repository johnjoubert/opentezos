var fs = require('fs');
const path = require('path');
var variables = require('../variables.json')

function injectVariables(file) {
  fs.readFile(file, 'utf-8', function(err, data){
    if (err) throw err;

    var newValue = data.replace(/<:([A-Z_]+):>/g, (match, varName) => {
        return variables[varName] || varName;
      }
    );
    
    fs.writeFile(file, newValue, 'utf-8', function (err) {
      if (err) throw err;
    });
  });
}

function throughDirectory(directory) {
    fs.readdirSync(directory).forEach(file => {
        const absolutePath = path.join(directory, file);
        if (fs.statSync(absolutePath).isDirectory()) return throughDirectory(absolutePath);
        else {
            if (path.extname(absolutePath) == '.md') injectVariables(absolutePath);
        }
    });
}

/**
 * @param {string} src  The path to the thing to copy.
 * @param {string} dest The path to the new copy.
 */
var copyRecursiveSync = function(src, dest) {
    var exists = fs.existsSync(src);
    var stats = exists && fs.statSync(src);
    var isDirectory = exists && stats.isDirectory();
    if (isDirectory) {
        fs.mkdirSync(dest);
        fs.readdirSync(src).forEach(function(childItemName) {
        copyRecursiveSync(path.join(src, childItemName),
                            path.join(dest, childItemName));
        });
    } else {
        fs.copyFileSync(src, dest);
    }
};

// Clean previous prebuild
fs.rmSync(path.resolve(__dirname, '../prebuild/'), { recursive: true, force: true });

// Copy all files under ../docs/ to ../prebuild/
copyRecursiveSync(path.resolve(__dirname, '../docs/'), path.resolve(__dirname, '../prebuild/'));

// Apply the variable injection in every .md files
throughDirectory(path.resolve(__dirname, '../prebuild/'));
